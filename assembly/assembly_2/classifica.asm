		addi $v0,$zero,10
		
		addi $s1,$zero,-1
		addi $s2,$zero,32
		addi $s4,$zero,32


		bgez $s1,s1meqzero
		bltz $s1,s1lzero
		syscall

s1meqzero: 	addi $s3,$zero,1
		beq $s2,$s4,s2igual
		
		addi $s2,$s2,-32
		bltz $s2,s2lt
		
		syscall

s1lzero:	addi $s3,$zero,4
		beq $s2,$s4,s2igual
		
		addi $s2,$s2,-32
		bltz $s2,s2lt
		
		syscall


s2igual:	addi $s3,$s3,1

		syscall
		
s2lt:		addi $s3,$s3,2

		syscall