		addi $s1,$zero,0
		addi $s2,$zero,80
		
start_loading:	li $v0,5
		
		syscall
		
		move $s3,$v0
		
		sw $s3,0x10010000($s1)
		
		
		addi $s1,$s1,4
		
		bne $s1,$s2,start_loading
	
			
		addi $s1,$zero,0
		addi $s2,$zero,-80
		
start_printing:	lw $a0,0x10010050($s1)

		li $v0,1
		
		syscall
		
		addi $s1,$s1,-4
		
		bne $s1,$s2,start_printing
		