library ieee;
use ieee.std_logic_1164.all;

entity decoder is
    port(
        select_0 : in STD_LOGIC;
        select_1 : in STD_LOGIC;
        out_0: out STD_LOGIC;
        out_1: out STD_LOGIC;
        out_2: out STD_LOGIC;
        out_3: out STD_LOGIC
    );
end entity;

architecture a_decoder of decoder is
begin
    out_0 <= not select_0 and not select_1;
    out_1 <= select_0 and not select_1;
    out_2 <= not select_0 and select_1;
    out_3 <= select_0 and select_1;
end architecture;