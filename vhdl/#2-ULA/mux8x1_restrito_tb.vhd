library ieee;
use ieee.std_logic_1164.all;

entity mux8x1_tb is 
end entity mux8x1_tb;

architecture a_mux8x1_tb of mux8x1_tb is
    component mux8x1
        port(
            sel0, sel1, sel2    : in std_logic;
            in2, in4, in6       : in std_logic;
            out0                : out std_logic
        );
    end component;

    signal sel0, sel1, sel2, in2, in4, in6, out0: std_logic;
    begin 
        uut: mux8x1 port map(
        sel0 => sel0,
        sel1 => sel1,
        sel2 => sel2,
        in2  => in2,
        in4  => in4,
        in6  => in6
        );
    process
    begin
        sel0 <= '0';
        sel1 <= '0';
        sel2 <= '0';
        in2  <= '1';
        in4  <= '1';
        in6  <= '1';
        wait for 50 ns;
        sel0 <= '1';
        sel1 <= '0';
        sel2 <= '0';
        in2  <= '1';
        in4  <= '1';
        in6  <= '1';
        wait for 50 ns;
        sel0 <= '0';
        sel1 <= '1';
        sel2 <= '0';
        in2  <= '1';
        in4  <= '1';
        in6  <= '1';
        wait for 50 ns;
        sel0 <= '1';
        sel1 <= '1';
        sel2 <= '0';
        in2  <= '1';
        in4  <= '1';
        in6  <= '1';
        wait for 50 ns;

        sel0 <= '0';
        sel1 <= '0';
        sel2 <= '1';
        in2  <= '1';
        in4  <= '1';
        in6  <= '1';
        wait for 50 ns;

        sel0 <= '1';
        sel1 <= '0';
        sel2 <= '1';
        in2  <= '1';
        in4  <= '1';
        in6  <= '1';
        wait for 50 ns;

        sel0 <= '0';
        sel1 <= '1';
        sel2 <= '1';
        in2  <= '1';
        in4  <= '1';
        in6  <= '1';
        wait for 50 ns;

        sel0 <= '1';
        sel1 <= '1';
        sel2 <= '1';
        in2  <= '1';
        in4  <= '1';
        in6  <= '1';
        wait for 50 ns;
        wait;

    end process;
end architecture;
