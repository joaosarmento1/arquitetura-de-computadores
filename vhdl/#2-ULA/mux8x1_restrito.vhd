library ieee;
use ieee.std_logic_1164.all;

entity mux8x1 is
    port(
        sel0, sel1, sel2    : in std_logic;
        in2, in4, in6       : in std_logic;
        out0                : out std_logic
    );
end entity;

architecture a_mux8x1 of mux8x1 is
    begin
        out0 <=  '0' when sel0='0' and sel1='0' and sel2='0' else
                 '0' when sel0='1' and sel1='0' and sel2='0' else
                 in2 when sel0='0' and sel1='1' and sel2='0' else
                 '1' when sel0='1' and sel1='1' and sel2='0' else
                 in4 when sel0='0' and sel1='0' and sel2='1' else
                 '0' when sel0='1' and sel1='0' and sel2='1' else
                 in6 when sel0='0' and sel1='1' and sel2='1' else
                 '1' when sel0='1' and sel1='1' and sel2='1' else
                 '0';
    end architecture;
